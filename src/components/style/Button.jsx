import React from 'react'
import Styled from 'styled-components'


const Buttonn = Styled.button`

    color: white;
    background: blue;
    border: none;
    border-radius: 20px;
    margin: 1rem 0px;
    padding: 8px 2rem;
    cursor: pointer;

    :hover{

    background-image:linear-gradient(315deg, #1fd1f9 0%, #b621fe 74%);
    color: #ff04ff;
    font-style: bold;
    transform: scale(1.1);

    }
`;

const Button = () => {
  return (
    <Buttonn button>
    <div>Contact</div>
    </Buttonn>
  )
}

export default Button