import React from 'react'
import Styled from 'styled-components'

const Liste = Styled.ul`

    display: flex;
    justify-content: center;
    gap: 8rem;

    `;

    const Listitems = Styled.li`

    color: #f1eaea;
    list-style: none;
    cursor: pointer;
    display:flex;
    justify-content: space-between;
    flex-flow: row wrap;
    align-content:center;
    align-items: center;

    :hover{

    transform: scale(1.1);
    color: #ff04ff;

    }

`;

const List = () => {
  return (
    <Liste>
            <Listitems li>Services</Listitems>
            <Listitems li>Projects</Listitems>
            <Listitems li>About</Listitems>
    </Liste>
  )
}

export default List