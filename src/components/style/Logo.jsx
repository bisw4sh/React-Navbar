import React from 'react'
import Styled from 'styled-components';

const Logoo = Styled.div`

    font-weight: bold;
    color: #f1eaea;
    cursor: pointer;

    :hover{

    color: #ff00ff;
    box-shadow: 0 3px 15px -2px #ef8009;

    }
`;

const Logo = () => {
  return (
    <Logoo>
    <div>LOGOBAKERY</div>
    </Logoo>
  )
}

export default Logo