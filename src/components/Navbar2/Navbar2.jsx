import React from 'react'
import Styled from 'styled-components'
import Logo from '../style/Logo.jsx'
import List from '../style/List.jsx'
import Button from '../style/Button.jsx'

const Navbar = Styled.nav`
padding: 0px 4rem;
color: black;
background-color: #030303;
display: flex;
flex-flow: row wrap;
justify-content: space-between;
gap: 4rem;
align-content: center;
align-items: center;
text-align: center;

`;


const Navbar2 = () => {
  return (
   <Navbar>

    <Logo/>
    <List/>
    <Button/>

   </Navbar>
  )
}

export default Navbar2