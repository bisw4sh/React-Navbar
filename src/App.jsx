import { useState } from 'react'
import Styled from 'styled-components'
import Navbar1 from './components/Navbar1/Navbar1.jsx'
import Navbar2 from './components/Navbar2/Navbar2.jsx'
import Navbar3 from './components/Navbar3/Navbar3.jsx'

const Container = Styled.nav`

  height: 90vh;
  display: flex;
  justify-content: space-evenly;
  flex-flow: column wrap;
`;

function App() {

  return (
    <div className="App">
      <Container>

        <Navbar1/>
        <Navbar2/>
        <Navbar3/>

      </Container>
    </div>
  )
}

export default App